package ma.nemo.assignment.web;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Supply;
import ma.nemo.assignment.exceptions.ProductAlreadyExists;
import ma.nemo.assignment.exceptions.ProductValidationException;
import ma.nemo.assignment.exceptions.SupplyValidationException;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.SypplyRepository;
import ma.nemo.assignment.repository.TransactionHistoryRepository;
import ma.nemo.assignment.web.SupplyController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SupplyControllerTest {
    private SupplyController supplyController;

    @Autowired
    private SypplyRepository supplyRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    private Supply supply;

    private Product product;
    private ProductController productController;
    // methode pour initialiser les objets à utiliser dans les tests
    public void init() {
        if(supplyController==null){
            supplyController = new SupplyController();
        }
        if(supply==null){
            supply = new Supply();
        }
        if(product==null){
            product = new Product();
        }
        if(productController==null){
            productController = new ProductController();
        }
        // les autres objet seront instanciés grace à l'injection de dépendance
        productController.setTransactionRepo(transactionHistoryRepository);
        productController.setProductRepository(productRepository);
        supplyController.setSupplyRepository(supplyRepository);
        supplyController.setProductRepository(productRepository);
        supplyController.setTransactionHistoryRepository(transactionHistoryRepository);
    }

    @Test
    public void shouldCreateSupplyWithExistingProductCode() throws SupplyValidationException, ProductValidationException, ProductAlreadyExists {
        init();
        // create product that will be referenced in a supply
        product.setProductCode("Test1");
        product.setProductName("TestProduct  1");
        product.setDescription("Product for testing in supply handler");
        product.setUnitPrice(200.0);
        product.setQuantityInStock(5);
        // add the product to database in order to ensure that product exists
        ResponseEntity<Long> responseEntityPrd = productController.createProduct(product);
        // prepare supply object
        supply.setProductCode(product.getProductCode());
        supply.setQuantity(100);
        //
        ResponseEntity<Long> responseEntity = supplyController.createSupply(supply);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }
    @Test
    public void exceededQuantityShouldThrowSupplyValidationException() throws ProductValidationException, ProductAlreadyExists {
        init();
        product.setProductCode("Test2");
        product.setProductName("TestProduct  1");
        product.setDescription("Product for testing in supply handler");
        product.setUnitPrice(200.0);
        product.setQuantityInStock(5);

        ResponseEntity<Long> responseEntityPrd = productController.createProduct(product);

        supply.setProductCode("Test2");
        supply.setQuantity(501);
        assertThrows(SupplyValidationException.class, () -> supplyController.createSupply(supply));
    }





}
