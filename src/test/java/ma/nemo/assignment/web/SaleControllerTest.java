package ma.nemo.assignment.web;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Sale;
import ma.nemo.assignment.exceptions.ProductInStockNotSufficientException;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.SupplyValidationException;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.SaleRepository;
import ma.nemo.assignment.repository.TransactionHistoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SaleControllerTest {

    private Sale sale;
    @Autowired
    private SaleRepository saleRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    private SaleController saleController;
    private Product product;

    // pour initialiser les objets qui vont nous servir le long de notre processus de testing
    public void init(){
        if(sale==null){
            sale = new Sale();
        }
        if(product==null){
            product = new Product();
        }
        if(saleController==null){
            saleController = new SaleController();
            saleController.setSaleRepository(saleRepository);
            saleController.setProductRepository(productRepository);
            saleController.setTransactionHistoryRepository(transactionHistoryRepository);
        }
    }
    // Pour tester le cas ou le produit n'est pas disponible
    @Test
    public void shouldThrowProductNotFoundException(){
        init();
        // le code Test3 n'existe pas dans la base de donnée
        sale.setProductCode("Test3");
        sale.setSoldQuantity(1);
        // Dans ce cas l'exception levée doit etre ProductNotFound Exception
        assertThrows(ProductNotFound.class, () -> saleController.createSale(sale));

    }
    // Pour tester le cas ou la quantité en stock n'est pas suffisante
    @Test
    public void shouldThrowProductInStockNotSufficientException(){
        init();
        // préparer un produit pour s'assurer qu'il existe
        product.setProductCode("Test1");
        product.setProductName("TestProduct  1");
        product.setDescription("Product for testing in sale handler");
        product.setUnitPrice(200.0);
        product.setQuantityInStock(5);
        Product prd = productRepository.save(product);
        System.out.println(prd);
        // fixer une quantité plus grande que la quantité en stock
        sale.setProduct(product);
        sale.setProductCode("Test1");
        sale.setSoldQuantity(6);
        assertThrows(ProductInStockNotSufficientException.class, () -> saleController.createSale(sale));

    }

}