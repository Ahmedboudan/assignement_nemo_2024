package ma.nemo.assignment.web;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Return;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.SupplyValidationException;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.repository.ReturnRepository;
import ma.nemo.assignment.repository.TransactionHistoryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReturnControllerTest {
    @Autowired
    private ReturnRepository returnRepository;
    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;
    @Autowired
    private ProductRepository productRepository;
    private ReturnController returnController;
    private Return aReturn;

    private Product product;

    private void init(){
        if(aReturn==null){
            aReturn = new Return();
        }
        if(returnController==null){
            returnController = new ReturnController();
        }
        if(product==null){
            product = new Product();
        }
        returnController.setReturnRepository(returnRepository);
        returnController.setProductRepository(productRepository);
        returnController.setTransactionHistoryRepository(transactionHistoryRepository);
    }
    @Test
    public void shouldThrowProductNotFoundException(){
        init();
        // fournir un code qui n'existe pas
        aReturn.setProductCode("Test4");
        aReturn.setReturnQuantity(20);
        aReturn.setReasonForReturn("Product quality does not match");
        assertThrows(ProductNotFound.class, () -> returnController.createReturn(aReturn));
    }
    @Test
    public void returningWithExistingProductCodeShouldSucceed() throws ProductNotFound {
        init();
        // préparer un produit qui existe
        product.setProductCode("Test5");
        product.setProductName("TestProduct  1");
        product.setDescription("Product for testing in supply handler");
        product.setUnitPrice(200.0);
        product.setQuantityInStock(5);
        productRepository.save(product);

        aReturn.setProductCode("Test5");
        aReturn.setReturnQuantity(22);
        aReturn.setReasonForReturn("Product quality does not match");
        ResponseEntity responseEntity = returnController.createReturn(aReturn);
        assertEquals(HttpStatus.CREATED,responseEntity.getStatusCode());
    }
}